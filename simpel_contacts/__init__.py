from simpel_contacts.version import get_version

VERSION = (0, 1, 8, "final", 0)

__version__ = get_version(VERSION)
