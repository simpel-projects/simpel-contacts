from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from simpel_contacts.admin import (
    BillingAddressInline, DeliverableAddressInline, LinkedAddressInline, LinkedContactInline, ShippingAddressInline,
)

from .models import Order, Partner, PartnerAddress, Project

User = get_user_model()

admin.site.unregister(User)


class PartnerAddressInline(admin.StackedInline):
    model = PartnerAddress
    extra = 0


@admin.register(Partner)
class PartnerAdmin(admin.ModelAdmin):
    inlines = [PartnerAddressInline]


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    inlines = [BillingAddressInline, ShippingAddressInline]


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    inlines = [DeliverableAddressInline]


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    inlines = [LinkedContactInline, LinkedAddressInline]
