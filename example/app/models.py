from django.db import models

from simpel_contacts.abstracts import AbstractAddress


class Partner(models.Model):

    fullname = models.CharField("Fullname", max_length=255)


class PartnerAddress(AbstractAddress):
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE)


class Order(models.Model):
    po_number = models.CharField("PO Number", max_length=255)


class Project(models.Model):
    title = models.CharField("title", max_length=255)
